Ansible checkmk Agent Role
=========

An Ansible role to install and configure the zoneminder.

Requirements
------------

None

Role Variables
--------------

This playbook exposes the following configuration variables:

* `mysql_root_password` - Root password for the mysql/mariadb server

Dependencies
------------

None.

Example Playbook
----------------

User the role as follows:

```yaml
  - hosts: servers
    roles:
      - role: zoneminder
        mysql_root_password: supertopsecret
```

License
-------

AGPLv3

Author Information
------------------

Rob Connolly [https://webworxshop.com](https://webworxshop.com)
